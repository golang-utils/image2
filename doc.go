// Copyright (c) 2023 Marc René Arns. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.

/*
Package image2 fixes problems of the image package within the standard library with
decoding JPEG images that are corrupt or broken.

For discussion, see: https://github.com/golang/go/issues/10447

This patch is mainly due to the suggestion of  "whorfin" in
this comment: https://github.com/golang/go/issues/10447#issuecomment-843447295
and some code and tests of mine

Usage is like the normal jpeg sub package of image, just with a different path
(see the example test).
*/
package image2
